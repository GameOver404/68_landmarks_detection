
import dlib
import cv2
import pandas as pd
from imutils import face_utils
import numpy as np
import imutils

# get coordinates from the face
def rect_to_bb(rect):
    x = rect.left()
    y = rect.top()
    w = rect.right() - x
    h = rect.bottom() - y

    return (x,y,w,h)

# transform the shape to a NumPy array
def shape_to_np(shape, dtype="int"):
	coords = np.zeros((68, 2), dtype=dtype)
	# change the 68 landmarks to their (x,y) representation
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)
	return coords


def addLandmarksToImage(image, rects, predictor):
	# for every face
	for (i, rect) in enumerate(rects):
		# find face landmarks + convert them to a NumPy array
		shape = predictor(image, rect)
		shape = face_utils.shape_to_np(shape)
		# draw facial landmarks
		for (x, y) in shape:
			cv2.circle(image, (x, y), 1, (0, 0, 255), -1)
		# show blue rectangle around the face
		(x, y, w, h) = face_utils.rect_to_bb(rect)
		cv2.rectangle(image, (x, y), (x + w, y + h), (0,191,255), 2)

