import dlib
import cv2
import pandas as pd
import landmarks_helper as lh
from imutils import face_utils
import numpy as np

# use dlibs pretrained models for face and 68 landmarks detection
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# start webcam video capture
cam = cv2.VideoCapture(0)

if not cam.isOpened():
    raise IOError("Cannot open webcam.")

while True:
    # capture and prepare the frame
    ret_val, img = cam.read()
    img = cv2.flip(img, 1)
    img = imutils.resize(img, width=600, height=400)
    # detect faces
    rects = detector(img, 1)
    # detect landmarks
    lh.addLandmarksToImage(img, rects, predictor)
    # show output
    cv2.imshow('Output', img)

    if cv2.waitKey(1) == 27: 
        cv2.destroyAllWindows()

# training
#dataset = pd.read_csv('train_data.csv', header=None)

#images = []
#for index, row in dataset.iterrows():
#    image = np.reshape(np.asarray(row), (48, 48))
#    images.append(image)
    #cv2.imwrite('images/img_' + str(index) + '.jpg', image)


