from flask import Flask, render_template, Response, url_for
import cv2
import sys
import numpy
import landmarks_helper as lh
import dlib
import imutils

app = Flask(__name__)

# use dlibs pretrained models
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# start the app -> go to index / home
@app.route('/')
@app.route('/index.html')
def face():
    return render_template('index.html')

# capture frames
def get_frame():

    ramp_frames=100
    # 0 = webcam, video file path would be possible as well
    camera=cv2.VideoCapture(0) 
    i=1
    while True:
        # capture frame and resize it
        retval, im = camera.read()
        im = imutils.resize(im, width=400, height=400)
        # detect faces
        rects = detector(im, 1)
        # detect landmarks
        lh.addLandmarksToImage(im, rects, predictor)
        # transofrm image
        imgencode=cv2.imencode('.jpg',im)[1]
        stringData=imgencode.tostring()
        yield (b'--frame\r\n'
            b'Content-Type: text/plain\r\n\r\n'+stringData+b'\r\n')
        i+=1
    # closes the camera once the VideoCapture ended
    del(camera)

# calculate the output
@app.route('/calc')
def calc():
     return Response(get_frame(),mimetype='multipart/x-mixed-replace; boundary=frame')

# start the app
if __name__ == '__main__':
    app.run(host='localhost', debug=False, threaded=True)
