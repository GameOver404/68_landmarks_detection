# Facial-Landmarks-Detection App

- Built using Dlibs shape predictor for the 68 facial landmarks
- With the possibility to retrain it via cvs-files thanks to pandas.


## Prerequisites

This project requires the following dependencies to be installed beforehand:

* Python and pip 
* vs2017 for Windows
* CMake
* xQuartz/X11 

# Dependencies

- flask
- dlib
- imutils
- opencv-python
- pandas
- numpy

To install all the needed dependencies run:
```
pip install -r requirements.txt
```

# Usage

- Once the dependencies are installed (via pip) start the app with:
```
python app.py
```